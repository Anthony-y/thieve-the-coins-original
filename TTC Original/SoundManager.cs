﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ECS_Demo.Managers
{
    public class SoundManager
    {
        // TODO: Finsish

        private Entity entity;
        private System system;

        private List<SoundEffect> soundEffects;

        public SoundManager(Entity entity) { this.entity = entity; soundEffects = new List<SoundEffect>(); }
        public SoundManager(System system) { this.system = system; soundEffects = new List<SoundEffect>(); }
        public SoundManager(Entity entity, System system) { this.entity = entity; this.system = system;
            soundEffects = new List<SoundEffect>();
        }

        public void AddSoundEffect(SoundEffect soundEffect) { soundEffects.Add(soundEffect); }

    }
}
