﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ECS_Demo.Components;

namespace ECS_Demo.Systems
{
    class AIPatrolSystem : System
    {
        private EntityManager enityManager;
        private CAIPatrol aiPatrol;
        private Game1 game;

        public AIPatrolSystem(EntityManager enityManager, CAIPatrol aiPatrol, Game1 game)
        {
            SetTag("AIPatrolSystem");

            this.game = game;
            this.enityManager = enityManager;
            this.aiPatrol = aiPatrol;
        }

        public override void Update(GameTime gameTime)
        {
            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent<CCollider>("CCollider").GetCollider().
                Intersects(enityManager.GetEntityFromTag("Player").GetComponent<CCollider>("CCollider").GetCollider()))
            {
                MessageBox.Show("You lose!");
                game.Exit();
            }

            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent<CTransform>("CTransform").
                GetPositionVector().X <= 0 && aiPatrol.GetPatrolType() == true)
            {
                aiPatrol.SetDirection(EDirection.right);
            }

            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent<CTransform>("CTransform").
                GetPositionVector().X >= 640 - 32 && aiPatrol.GetPatrolType() == true)
            {
                aiPatrol.SetDirection(EDirection.left);
            }

            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent<CTransform>("CTransform").
                GetPositionVector().Y <= 0 && aiPatrol.GetPatrolType() == false)
            {
                aiPatrol.SetDirection(EDirection.down);
            }

            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent<CTransform>("CTransform").
                GetPositionVector().Y >= 480 - 32 && aiPatrol.GetPatrolType() == false)
            {
                aiPatrol.SetDirection(EDirection.up);
            }

            base.Update(gameTime);
        }

    }
}
