﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ECS_Demo.Components;
using Microsoft.Xna.Framework;

namespace ECS_Demo.Systems
{
    public class HouseEnterSystem : System
    {
        private CTransform playerTransform;
        private CTransform houseTransform;
        private Rectangle playerCollider;
        private Rectangle houseCollider;

        public HouseEnterSystem(Entity house, Entity player)
        {
            SetTag("HouseEnterSystem");

            playerCollider = player.GetComponent<CCollider>("CCollider").GetCollider();
            houseCollider = house.GetComponent<CCollider>("CCollider").GetCollider();
        }

        public override void Update(GameTime gameTime)
        {
            if (playerCollider.Intersects(houseCollider))
            {
                Console.WriteLine("Collision!");
            }

            base.Update(gameTime);
        }

    }
}
