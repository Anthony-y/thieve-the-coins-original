﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using ECS_Demo.Components;
using ECS_Demo.Managers;

namespace ECS_Demo.Systems
{
    public class CoinPickupSystem : System
    {
        //private bool hasCached = false;

        private CCoinPickup coinPickup;
        private EntityManager entityManager;
        private SystemsManager systemsManager;
        private Random rand;
        private Game1 game;

        private SoundEffect youWonSound;
        private SoundEffect coinPickupSound;

        private SpriteFont font;

        private int winningScore = 10;
        private bool hasWon = false;

        public CoinPickupSystem(CCoinPickup coinPickup, EntityManager entityManager, SystemsManager systemsManager,
            Game1 game, ContentManager content)
        {
            SetTag("CoinPickupSystem");

            this.game = game;
            this.entityManager = entityManager;
            this.systemsManager = systemsManager;
            this.coinPickup = coinPickup;
            rand = new Random();

            font = content.Load<SpriteFont>("Arial");
            youWonSound = content.Load<SoundEffect>("You Won!");
            coinPickupSound = content.Load<SoundEffect>("CoinPickup");
        }

        public override void Update(GameTime gameTime)
        {
            if (coinPickup.GetScore() >= winningScore)
            {
                youWonSound.Play();
                MessageBox.Show("You won!");
                game.Exit();
            }

            if (!hasWon && entityManager.GetEntityFromTag(coinPickup.GetOwner().GetTag()).GetComponent<CCollider>("CCollider").GetCollider().Intersects(
                entityManager.GetEntityFromTag("Coin").GetComponent<CCollider>("CCollider").GetCollider()))
            {
                coinPickupSound.Play();
                coinPickup.AddScore(1);
                Console.WriteLine(coinPickup.GetScore());
                entityManager.GetEntityFromTag("Coin").GetComponent<CTransform>("CTransform").SetPositionVector(
                    new Vector2(rand.Next(100, 301), rand.Next(100, 301)));
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, "Score: " + coinPickup.GetScore() 
                + "/" + winningScore, new Vector2(640 / 2 - 32, 0), Color.White);

            base.Draw(spriteBatch);
        }


    }
}
