﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECS_Demo
{
    public class System
    {
        private string tag;

        public System() { }
        public virtual void Update(GameTime gameTime) { }
        public virtual void Draw(SpriteBatch spriteBatch) { }
        public string GetTag() { return tag; }
        public void SetTag(string tag) { this.tag = tag; }
    }
}
