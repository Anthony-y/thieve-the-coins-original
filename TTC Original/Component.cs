﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECS_Demo
{
    public class Component
    {
        private string tag;
        private Entity owner;

        public Component(Entity owner) { this.owner = owner; }
        public virtual void Update(GameTime gameTime) { }
        public virtual void Draw(SpriteBatch spriteBatch) { }
        public void SetTag(string tag) { this.tag = tag; }
        public string GetTag() { return tag; }
        public Entity GetOwner() { return owner; }
    }
}
