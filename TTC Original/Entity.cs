﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ECS_Demo
{
    public class Entity
    {
        private List<Component> components;
        private string tag;

        public Entity() { components = new List<Component>(); }
        public void AddComponent(Component c) { components.Add(c); }
        public void Update(GameTime gameTime)
        {
            foreach (Component c in components)
            {
                c.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Component c in components)
            {
                c.Draw(spriteBatch);
            }
        }

        public T GetComponent<T>(string componentTag) where T : Component
        {
            try
            {
                var match = (T)components.FirstOrDefault(com => com.GetTag().Contains(componentTag));
                return match;
            }

            catch (NullReferenceException)
            {
                Console.WriteLine("Couldn't find Component of tag/type: " + componentTag);
                return null; 
            }

        }

        public List<Component> GetAllComponents() { return components; }
        public string GetTag() { return tag; }
        public void SetTag(string tag) { this.tag = tag; }

    }
}
