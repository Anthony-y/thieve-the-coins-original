﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECS_Demo.Managers
{
    public class SystemsManager
    {
        private List<System> systems;
        private System system;
        private string systemTag;

        public const int MAXSYSTEMS = 50;

        public SystemsManager()
        {
            systems = new List<System>();
        }

        public void SetupSystem(System system, string tag)
        {
            if (systems.Count() < MAXSYSTEMS)
            {
                this.system = system;
                systemTag = tag;
                this.system = system;
                systems.Add(system);
            }

            else { Console.WriteLine("The amount of systems exceeds the maximum allowed amount!"); }
        }

        public void UpdateSystems(GameTime gameTime)
        {
            foreach (System sys in systems)
            {
                sys.Update(gameTime);
            }
        }

        public void DrawSystems(SpriteBatch spriteBatch)
        {
            foreach (System sys in systems)
            {
                sys.Draw(spriteBatch);
            }
        }

        public System GetSystemFromTag(string tagToGet)
        {
            var match = systems.FirstOrDefault(sys => sys.GetTag() == tagToGet);
            return match;
        }

        public void RemoveSystemFromTag(string tagToGet)
        {
            var match = systems.FirstOrDefault(sys => sys.GetTag() == tagToGet);
            systems.Remove(match);
        }

    }
}
