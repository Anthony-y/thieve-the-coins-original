﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace ECS_Demo
{
    public class EntityManager
    {
        private List<Entity> entityWorld;
        private List<Entity> cachedEntities;
        private Entity entity;
        private string tag;

        public const int MAXENTITIES = 100;

        public EntityManager() { entityWorld = new List<Entity>(); cachedEntities = new List<Entity>(); }

        public void SetupEntity(Entity entity, string tag, params Component[] components)
        {
            if (entityWorld.Count < MAXENTITIES)
            {
                this.tag = tag;
                this.entity = entity;
                entity.SetTag(tag);
                foreach (Component c in components) { entity.AddComponent(c); }
                entityWorld.Add(entity);
            }

            else { Console.WriteLine("The amount of entities exceeds the maximum allowed amount!"); }
        }

        public string GetTag() { return tag; }

        public void UpdateEntities(GameTime gameTime)
        {
            foreach (Entity e in entityWorld) { e.Update(gameTime); }
        }

        public void DrawEntities(SpriteBatch spriteBatch)
        {
            foreach (Entity e in entityWorld) { e.Draw(spriteBatch); }
        }

        /// <summary> Removes the entity with the tag exactly "tagToRemove" </summary>
        public void RemoveEntityFromTag(string tagToRemove)
        {
            try
            {
                var match = entityWorld.FirstOrDefault(ent => ent.GetTag() == tagToRemove);
                entityWorld.Remove(match);
            }

            catch(NullReferenceException) { Console.WriteLine("Couldn't find Entity of tag: " + tagToRemove); }

        }

        /// <summary> Returns an entity with the tag exactly "tagToGet". </summary>
        public Entity GetEntityFromTag(string tagToGet)
        {
            try
            {
                var match = entityWorld.FirstOrDefault(ent => ent.GetTag() == tagToGet);
                return match;
            }

            catch(NullReferenceException) { Console.WriteLine("Couldn't find Entity of tag: " + tagToGet); return null; }

        }
        
        public void AddComponent(string entityTag, Component c)
        {
            GetEntityFromTag(entityTag).AddComponent(c);
        }

        /// <summary> Caches an entity and it's components for later use. </summary>
        public void CacheEntity(string entityTag)
        {
            var ent = GetEntityFromTag(entityTag);
            cachedEntities.Add(ent);
            Console.WriteLine("Cached " + ent.GetTag());
        }

        /// <summary> Retrieves an already cachec entity. </summary>
        public void GetFromCache(string entityTag)
        {
            RemoveEntityFromTag(entityTag);
            var match = cachedEntities.FirstOrDefault(ent => ent.GetTag() == entityTag);
            SetupEntity(match, match.GetTag(), match.GetAllComponents().ToArray());
            Console.WriteLine("Retrieved " + match.GetTag() + " from cache.");
        }

        public void RemoveFromCache(string entityTag)
        {
            var match = cachedEntities.FirstOrDefault(ent => ent.GetTag() == entityTag);
            cachedEntities.Remove(match);
        }

        public void CreateEntityWorld(List<Entity> newWorld)
        {
            newWorld = new List<Entity>();
        }

        public Entity LoadWorldFromFile(string filePath)
        {
            using (var fileStream = new FileStream(filePath, FileMode.Open))
            {
                XmlSerializer xmlS = new XmlSerializer(typeof(Entity), "D:/Magic/C#/ProjectWTF/ProjectWTF");
                var result = xmlS.Deserialize(fileStream);
                Console.WriteLine(result);
                return (Entity)result;
            }
        }
    }
}
