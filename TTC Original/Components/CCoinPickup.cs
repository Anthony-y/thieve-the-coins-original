﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ECS_Demo.Components
{
    public class CCoinPickup : Component
    {
        private int score = 0;

        public CCoinPickup(Entity owner) 
            : base(owner)
        {
            SetTag("CCoinPickup");
        }

        public int GetScore() { return score; }
        public void SetScore(int newScore) { newScore = score; }
        public void AddScore(int add) { score += add; }
        public void RemoveScore(int remove) { score -= remove; }
    }
}
