﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ECS_Demo.Components
{
    class CWASDControl : Component
    {
        private Entity owner;
        private CTransform transform;
        private KeyboardState kb;
        private KeyboardState prevKb;
        private EDirection direction;

        private float moveSpeed = 200;

        public CWASDControl(Entity owner)
            : base(owner)
        {
            SetTag("CWASDControl");
            this.owner = owner;
            transform = owner.GetComponent<CTransform>("CTransform");
        }

        public override void Update(GameTime gameTime)
        {
            kb = Keyboard.GetState();

            if (kb.IsKeyDown(Keys.W) || kb.IsKeyDown(Keys.Up)) { direction = EDirection.up;
                transform.RemoveFromPositionY(moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds); }

            if (kb.IsKeyDown(Keys.S) || kb.IsKeyDown(Keys.Down)) { direction = EDirection.down;
                transform.AddToPositionY(moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds); }

            if (kb.IsKeyDown(Keys.A) || kb.IsKeyDown(Keys.Left)) { direction = EDirection.left;
                transform.RemoveFromPositionX(moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds); }

            if (kb.IsKeyDown(Keys.D) || kb.IsKeyDown(Keys.Right)) { direction = EDirection.right;
                transform.AddToPositionX(moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds); }

            prevKb = kb;

            base.Update(gameTime);
        }

        public void SetMoveSpeed(float moveSpeed) { this.moveSpeed = moveSpeed; }
        public float GetMoveSpeed() { return moveSpeed; }
        public KeyboardState GetKeyBoardState() { return kb; }
        public KeyboardState GetPrevKeyBoardState() { return prevKb; }
        public EDirection GetDirection() { return direction; }
    }
}
