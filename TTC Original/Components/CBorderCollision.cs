﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ECS_Demo.Components
{
    class CBorderCollision : Component
    {
        private CTransform transform;
        private Entity owner;

        public CBorderCollision(Entity owner)
            : base(owner)
        {
            SetTag("CBorderCollision");
            this.owner = owner;
            transform = owner.GetComponent<CTransform>("CTransform");
        }

        public override void Update(GameTime gameTime)
        {
            // 32 is the width and height of the player sprite.
            if (transform.GetPositionVector().X <= 0) { transform.SetPositionX(0); }
            if (transform.GetPositionVector().X >= 640 - 32) { transform.SetPositionX(640 - 32); }

            if (transform.GetPositionVector().Y <= 0) { transform.SetPositionY(0); }
            if (transform.GetPositionVector().Y >= 480 - 32) { transform.SetPositionY(480 - 32); }

            base.Update(gameTime);
        }

    }
}
