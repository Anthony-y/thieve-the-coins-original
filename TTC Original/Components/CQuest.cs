﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ECS_Demo.Components
{
    class CQuest : Component
    {
        private Entity player;
        private Entity owner;
        private Rectangle playerCollider;
        private Rectangle ownerCollider;
        private CTransform playerPos;
        private Vector2 ownerPos;
        private string introLine;

        public CQuest(Entity owner, Entity player)
            : base(owner)
        {
            this.player = player;
            this.owner = owner;

            playerCollider = player.GetComponent<CCollider>("CCollider").GetCollider();
            ownerCollider = owner.GetComponent<CCollider>("CCollider").GetCollider();

            ownerPos = owner.GetComponent<CTransform>("CTransform").GetPositionVector();
            playerPos = player.GetComponent<CTransform>("CTransform");

        }

        public void SetQuestLine(string introLine) { this.introLine = introLine; }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

    }
}
