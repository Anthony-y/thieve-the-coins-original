﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ECS_Demo.Components
{
    class CAIPatrol : Component
    {
        private bool isX;
        private float moveSpeed = 700;
        private EDirection direction;
        private CTransform transform;
        private Entity owner;

        public CAIPatrol(Entity owner, bool isX) 
            : base(owner)
        {
            SetTag("CAIPatrol");
            this.owner = owner;
            this.isX = isX;
            transform = owner.GetComponent<CTransform>("CTransform");
        }

        public float GetMoveSpeed() { return moveSpeed; }

        public override void Update(GameTime gameTime)
        {
            if (direction == EDirection.left)
            {
                transform.RemoveFromPositionX(moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }

            if (direction == EDirection.right)
            {
                transform.AddToPositionX(moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }

            if (direction == EDirection.down)
            {
                transform.AddToPositionY(moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }

            if (direction == EDirection.up)
            {
                transform.RemoveFromPositionY(moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }

            base.Update(gameTime);
        }

        public void SetMoveSpeed(float newSpeed) { moveSpeed = newSpeed; }
        public EDirection GetDirection() { return direction; }
        public void SetDirection(EDirection direction) { this.direction = direction; }
        public bool GetPatrolType() { return isX; }

    }
}
