﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ECS_Demo.Components
{
    class CCollider : Component
    {
        private CTransform transform;
        private CTexture texture;
        private Entity owner;
        private Rectangle collider;

        public CCollider(Entity owner)
            : base(owner)
        {
            SetTag("CCollider");
            this.owner = owner;
            texture = owner.GetComponent<CTexture>("CTexture");
            transform = owner.GetComponent<CTransform>("CTransform");
            collider = new Rectangle(0, 0, texture.GetTexture().Width, texture.GetTexture().Height);
        }
        public override void Update(GameTime gameTime)
        {
            collider.X = (int)transform.GetPositionVector().X;
            collider.Y = (int)transform.GetPositionVector().Y;

            base.Update(gameTime);
        }

        public Rectangle GetCollider() { return collider; }

    }
}
