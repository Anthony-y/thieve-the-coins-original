﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECS_Demo.Components
{
    public class CTransform : Component
    {
        private Vector2 position;
        private Vector2 scale;

        public CTransform(Entity owner, Vector2 position, Vector2 scale)
            : base(owner)
        {
            SetTag("CTransform");
            this.position = position;
            this.scale = scale;
        }

        public void SetPositionVector(Vector2 newVector) { position = newVector; }
        public Vector2 GetPositionVector() { return position; }

        public void SetPositionX(float newX) { position.X = newX; }
        public void SetPositionY(float newY) { position.Y = newY; }

        public void AddToPositionX(float add) { position.X += add; }
        public void AddToPositionY(float add) { position.Y += add; }
        public void RemoveFromPositionX(float remove) { position.X -= remove; }
        public void RemoveFromPositionY(float remove) { position.Y -= remove; }

        public void SetScale(Vector2 newScale) { scale = newScale; }
        public Vector2 GetScaleVector() { return scale; }

    }
}
