﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using ECS_Demo.Managers;
using ECS_Demo.Components;
using ECS_Demo.Systems;

using System.CodeDom.Compiler;

namespace ECS_Demo
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        EntityManager entityManager;
        SystemsManager systemsManager;

        //Texture2D lvl1;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.PreferredBackBufferWidth = 640;
            graphics.PreferredBackBufferHeight = 480;

            Content.RootDirectory = "Content";
            Window.AllowUserResizing = false;
        }

        protected override void Initialize()
        {
            IsMouseVisible = true;

            entityManager = new EntityManager();
            systemsManager = new SystemsManager();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            entityManager.SetupEntity(new Entity(), "Player");
            entityManager.SetupEntity(new Entity(), "EnemyX");
            entityManager.SetupEntity(new Entity(), "EnemyY");
            entityManager.SetupEntity(new Entity(), "Coin");

            Entity playerOwner = entityManager.GetEntityFromTag("Player");
            Entity enemyXOwner = entityManager.GetEntityFromTag("EnemyX");
            Entity enemyYOwner = entityManager.GetEntityFromTag("EnemyY");
            Entity coinOwner   = entityManager.GetEntityFromTag("Coin");

            entityManager.AddComponent("Player", new CTransform(playerOwner, new Vector2(50, 50), new Vector2(1, 1)));
            entityManager.AddComponent("Player", new CTexture(playerOwner,
                Content, "player", Color.White));
            entityManager.AddComponent("Player", new CBorderCollision(playerOwner));
            entityManager.AddComponent("Player", new CWASDControl(playerOwner));
            entityManager.AddComponent("Player", new CCollider(playerOwner));
            entityManager.AddComponent("Player", new CCoinPickup(playerOwner));

            entityManager.AddComponent("EnemyX", new CTransform(enemyXOwner, 
                new Vector2(0, graphics.PreferredBackBufferHeight / 2), new Vector2(1, 1)));
            entityManager.AddComponent("EnemyX", new CTexture(enemyXOwner, Content, "enemy", Color.White));
            entityManager.AddComponent("EnemyX", new CCoinPickup(enemyXOwner));
            entityManager.AddComponent("EnemyX", new CBorderCollision(enemyXOwner));
            entityManager.AddComponent("EnemyX", new CCollider(enemyXOwner));
            entityManager.AddComponent("EnemyX", new CAIPatrol(enemyXOwner, true));

            entityManager.AddComponent("EnemyY", new CTransform(enemyYOwner, 
                new Vector2(graphics.PreferredBackBufferWidth / 2, 0), new Vector2(1, 1)));
            entityManager.AddComponent("EnemyY", new CTexture(enemyYOwner, Content, "enemy", Color.White));
            entityManager.AddComponent("EnemyY", new CCoinPickup(enemyYOwner));
            entityManager.AddComponent("EnemyY", new CCollider(enemyYOwner));
            entityManager.AddComponent("EnemyY", new CBorderCollision(enemyYOwner));
            entityManager.AddComponent("EnemyY", new CAIPatrol(enemyYOwner, false));

            entityManager.AddComponent("Coin", new CTransform(coinOwner, new Vector2(120, 120), new Vector2(1, 1)));
            entityManager.AddComponent("Coin", new CTexture(coinOwner, Content, "coin", Color.White));
            entityManager.AddComponent("Coin", new CCollider(coinOwner));
            entityManager.AddComponent("Coin", new CBorderCollision(coinOwner));

            systemsManager.SetupSystem(new CoinPickupSystem(playerOwner.GetComponent<CCoinPickup>("CCoinPickup"),
                entityManager, systemsManager, this, Content), "CoinPickUpSystem");
            systemsManager.SetupSystem(new AIPatrolSystem(entityManager, enemyXOwner.GetComponent<CAIPatrol>("CAIPatrol")
                , this), "AIPatrolSystemX");
            systemsManager.SetupSystem(new AIPatrolSystem(entityManager, enemyYOwner.GetComponent<CAIPatrol>("CAIPatrol")
                , this), "AIPatrolSystemY");

            base.LoadContent();

        }

        protected override void Update(GameTime gameTime)
        {
            entityManager.UpdateEntities(gameTime);
            systemsManager.UpdateSystems(gameTime);

            if (Keyboard.GetState().IsKeyDown(Keys.Escape)) { Exit(); }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            //spriteBatch.Begin();
            //spriteBatch.Draw(lvl1, new Vector2(0, 0), Color.White);
            //spriteBatch.End();

            spriteBatch.Begin();
            entityManager.DrawEntities(spriteBatch);
            systemsManager.DrawSystems(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
